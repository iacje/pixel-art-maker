const width = 40;
const height = 20;
const colors = ["#001f3f", "#0074D9", "#7FDBFF", "#39CCCC", "#3D9970", "#2ECC40", "#FFDC00", "#FF851B", "#FF4136", "#85144b", "#F012BE", "#B10DC9", "#DDDDDD", "white"];
var currentColor = colors[1];

$.fn.changeColor = function changeColor(color) {
    $(this).css("background-color", color);
};

function drawGrid(width, height) {
    const $gridDiv = $("#grid");
    for (let row = 0; row < height ; row++) {
        const $rowDiv = $('<div class="divTableRow"></div>');
        for (let col = 0; col < width; col++) {
            const $cellDiv = $('<div class="divTableCell"></div>');
            $rowDiv.append($cellDiv);
        }
        $gridDiv.append($rowDiv);
    }
}

function drawPalette() {
    const $paletteDiv = $("#palette");
    colors.forEach(function(color) {
        const $colorDiv = $('<div class="color"></div>');
        $colorDiv.css("background-color", color);
        $paletteDiv.append($colorDiv);
    });
}

function drawCurrentColor() {
    const $currentDiv = $("#current");
    if (!$(".current").length) {
        const $para = $('<p class="current">Brush Color</p>')
        const $colorDiv = $('<div class="current color"></div>');
        $currentDiv.append($para);
        $currentDiv.append($colorDiv);
    }
    const $colorDiv = $('.current.color');
    $colorDiv.css({
        "background-color": currentColor,
        "border": "1px solid",
        "border-color": currentColor
    });
    if (currentColor == "white" || currentColor == "rgb(255, 255, 255)") {
        $colorDiv.css("border", "1px solid lightgray");
    }
}

function addHandlers() {
    /* https://stackoverflow.com/questions/7700066/check-if-mousedown-with-an-if-statement */
    let isDown = false;
    $('#grid').mousedown(function() {
        isDown = true;
    });
    $(document).mouseup(function() {
        isDown = false;  
    });
    $('.divTableCell').on("mouseenter", function() {
        if (isDown) {
            $(this).changeColor(currentColor);
        }
    });

    $('.divTableCell').on("mousedown", function() {
        $(this).changeColor(currentColor);
    });
    $('.color').on("click", function() {
        currentColor = $(this).css("background-color");
        drawCurrentColor();
    });
    $('#picker').on("change", function() {
        currentColor = $(this).val();
        drawCurrentColor();
    });
    $('#wheel').on("click", function() {
        $('#picker').click();
    });
    $('#clear p').on("click", function(event) {
        $('.divTableCell').changeColor("white");
    });
    /* https://stackoverflow.com/questions/12168734/save-content-of-div-with-local-storage-and-display-on-another-page */
    $('#save p').on("click", function() {
        localStorage.setItem('saved', JSON.stringify($("#grid").html()));
    });
    $('#load p').on("click", function() {
        const retrievedObject = localStorage.getItem('saved');
        if (retrievedObject !== null) {
            const $gridDiv = $("#grid");
            $gridDiv.html(JSON.parse(retrievedObject));
            addHandlers();
        }
        
    });
}

function main() {
    drawGrid(width, height);
    drawPalette();
    drawCurrentColor();
    addHandlers();
}

main();
